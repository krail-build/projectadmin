package uk.q3c.build.creator.git

import io.kotlintest.extensions.TestListener
import io.kotlintest.matchers.file.shouldContainFile
import io.kotlintest.matchers.file.shouldNotContainFile
import io.kotlintest.specs.BehaviorSpec
import io.mockk.every
import io.mockk.mockk
import org.apache.commons.io.FileUtils
import uk.q3c.build.creator.ProjectBuilder
import uk.q3c.build.creator.aws.TemporaryDirectoryListener
import uk.q3c.build.gitplus.local.GitLocal
import java.io.File

/**
 * Created by David Sowerby on 10 Oct 2018
 */
class GitKeepBuilderTest : BehaviorSpec() {
    override fun isInstancePerTest() = true
    val td = TemporaryDirectoryListener(this)

    override fun listeners(): List<TestListener> {
        return listOf(td)
    }

    init {
        Given("a directory structure with some empty and some used directories") {
            val dirB = File(td.tempDir, "a/b")
            val dirC = File(td.tempDir, "a/c")
            FileUtils.forceMkdir(dirB)
            FileUtils.forceMkdir(dirC)
            File(dirB, "test").createNewFile()

            When("we run the builder") {
                val builder = GitKeepBuilder()
                val local: GitLocal = mockk(relaxed = true)
                val projectCreator: ProjectBuilder = mockk(relaxed = true)
                every { projectCreator.local }.returns(local)
                every { local.projectDir() }.returns(td.tempDir)
                builder.projectCreator(projectCreator)
                builder.execute()

                Then("the empty directories ha a gitkeep file, but the others do not") {
                    dirB.shouldContainFile("test")
                    dirB.shouldNotContainFile(".gitkeep")
                    dirC.shouldContainFile(".gitkeep")
                }

            }
        }
    }
}