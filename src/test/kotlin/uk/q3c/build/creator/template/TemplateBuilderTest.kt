package uk.q3c.build.creator.template

import io.kotlintest.extensions.TestListener
import io.kotlintest.matchers.collections.shouldContainExactly
import io.kotlintest.matchers.file.shouldContainFile
import io.kotlintest.matchers.file.shouldNotContainFile
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.specs.BehaviorSpec
import io.mockk.every
import io.mockk.mockk
import org.apache.commons.io.FileUtils
import uk.q3c.build.creator.EmptyTemplateData
import uk.q3c.build.creator.ProjectBuilder
import uk.q3c.build.creator.aws.TemporaryDirectoryListener
import java.io.File

/**
 * Created by David Sowerby on 27 Sep 2018
 */
class TemplateBuilderTest : BehaviorSpec() {

    override fun isInstancePerTest() = true
    val td = TemporaryDirectoryListener(this)

    override fun listeners(): List<TestListener> {
        return listOf(td)
    }

    val builder = TemplateBuilder()
    val projectBuilder: ProjectBuilder = mockk(relaxed = true)
    val s3SourceBucket = "kayman-maven"
    val s3SourcePath = "common/build/script"

    init {

        Given("configuration steps") {
            val t1 = Template(sourcePath = "common", sourceFileName = "code.gradle", targetDir = td.tempDir, templateData = EmptyTemplateData())
            val t2 = Template(sourcePath = "common", sourceFileName = "text.gradle", targetDir = td.tempDir, templateData = EmptyTemplateData())


            When("configured") {
                builder.configParam(t1)
                builder.configParam(t2)

                Then("the steps are retained") {
                    builder.templates.shouldContainExactly(t1, t2)
                }
            }
        }

        Given("A configuration step with no modifications") {
            every { projectBuilder.templateCacheDirectory } returns File("src/test/resources")
            val expected = "uk.q3c.krail.DefaultBindingsCollator"
            val wholeLine = "collator: \${data.collatorReference}"
            val templateData = EmptyTemplateData()
            val t1 = Template(sourcePath = "service/ui", sourceFileName = "krail-bootstrap.yml", targetDir = td.tempDir, templateData = templateData)


            When("configured and executed ") {
                builder.configParam(t1)
                builder.projectCreator(projectBuilder)
                every { projectBuilder.s3BucketName }.returns(s3SourceBucket)
                every { projectBuilder.s3BucketPath }.returns(s3SourcePath)
                builder.execute()

                Then("the template is copied to the target") {
                    td.tempDir.shouldContainFile("krail-bootstrap.yml")

                }

                Then("File content should be changed") {
                    val f = File(td.tempDir, "krail-bootstrap.yml")
                    val s = FileUtils.readFileToString(f)
                    s.shouldContain(wholeLine)
                }
            }
        }

        Given("A configuration step with modifications and name change") {
            every { projectBuilder.templateCacheDirectory } returns File("src/test/resources")
            val expected = "uk.q3c.krail.DefaultBindingsCollator"
            val wholeLine = "collator: $expected"
            val templateData = KrailBootstrapYmlTemplateData(collatorReference = expected)
            val t1 = Template(sourcePath = "service/ui", sourceFileName = "krail-bootstrap.yml", targetDir = td.tempDir, targetFileName = "wiggly.gradle", templateData = templateData)

            When("configured and executed ") {
                builder.configParam(t1)
                builder.projectCreator(projectBuilder)
                every { projectBuilder.s3BucketName }.returns(s3SourceBucket)
                every { projectBuilder.s3BucketPath }.returns(s3SourcePath)
                builder.execute()

                Then("the template is copied to the target with new name") {
                    td.tempDir.shouldContainFile("wiggly.gradle")
                    td.tempDir.shouldNotContainFile("krail-bootstrap.yml")

                }

                Then("File content should be changed") {
                    val f = File(td.tempDir, "wiggly.gradle")
                    val s = FileUtils.readFileToString(f)
                    s.shouldContain(wholeLine)
                }
            }
        }
    }


}