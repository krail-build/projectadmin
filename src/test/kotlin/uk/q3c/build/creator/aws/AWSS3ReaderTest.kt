package uk.q3c.build.creator.aws

import io.kotlintest.extensions.TestListener
import io.kotlintest.matchers.collections.shouldContain
import io.kotlintest.matchers.file.shouldExist
import io.kotlintest.shouldThrow
import io.kotlintest.specs.BehaviorSpec
import org.apache.commons.io.FileUtils
import uk.q3c.build.creator.template.AWSDownloadException
import uk.q3c.build.creator.template.AWSDownloadTargetException
import uk.q3c.build.creator.template.S3ObjectDescriptor
import java.io.File

/**
 * Created by David Sowerby on 26 Sep 2018
 */
class AWSS3ReaderTest : BehaviorSpec() {

    override fun isInstancePerTest() = true
    val td = TemporaryDirectoryListener(this)

    override fun listeners(): List<TestListener> {
        return listOf(td)
    }

    val reader = AWSS3Reader()
    val bucketName = "kayman-maven"
    val sourceFolder = "common/build/script/common"

    init {
        Given("a valid file specification") {
            val source = S3ObjectDescriptor(bucket = bucketName, sourceFolder = sourceFolder, name = "core.gradle")

            When("we download it from S3") {
                reader.downloadFile(source, td.tempDir)

                Then("it is written to file") {
                    File(td.tempDir, "core.gradle").shouldExist()
                }
            }

            When("we download and rename it") {
                reader.downloadFile(source = source, targetDir = td.tempDir, targetFileName = "build.gradle")

                Then("the new file has the correct name") {
                    File(td.tempDir, "build.gradle").shouldExist()
                }
            }
        }

        Given("a source that does not exist") {
            val source = S3ObjectDescriptor(bucket = bucketName, sourceFolder = sourceFolder, name = "wiggly.rubbish")

            When("we download it from S3") {
                Then("it should throw") {
                    shouldThrow<AWSDownloadException> { reader.downloadFile(source, td.tempDir) }
                }
            }
        }

        Given("a target directory that does not exist") {
            val source = S3ObjectDescriptor(bucket = bucketName, sourceFolder = sourceFolder, name = "core.gradle")

            When("we download it from S3") {
                Then("it should throw") {
                    shouldThrow<AWSDownloadTargetException> { reader.downloadFile(source, File(td.tempDir, "non-existentDir")) }
                }
            }
        }
    }
}

class AWSS3FolderReaderTest : BehaviorSpec() {

    override fun isInstancePerTest() = true
    val td = TemporaryDirectoryListener(this)

    override fun listeners(): List<TestListener> {
        return listOf(td)
    }

    init {
        Given("a bucket and folder") {
            val bucketName = "kayman-maven"
            val folderName = "common/build/script"
            val targetDir = File(td.tempDir, "s3Download")


            When("it is downloaded") {
                FileUtils.forceMkdir(targetDir)
                AWSS3Reader().downloadFolder(bucketName, folderName, targetDir)

                Then("we have a folder of files") {
                    targetDir.listFiles().asList().shouldContain(File("${targetDir}/common"))
                }
            }
        }
    }
}