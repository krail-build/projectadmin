package uk.q3c.build.creator.aws

import io.kotlintest.Description
import io.kotlintest.Spec
import io.kotlintest.TestResult
import io.kotlintest.extensions.TestListener
import io.kotlintest.specs.BehaviorSpec
import org.apache.commons.io.FileUtils
import java.io.File

/**
 * Created by David Sowerby on 26 Sep 2018
 */
class ReaderTest : BehaviorSpec(), TestListener {
    override fun listeners(): List<TestListener> = listOf(TemporaryDirectoryListener(this))

    override fun isInstancePerTest() = true

    override fun afterDiscovery(descriptions: List<Description>) {
        println("After discovery")
    }

    override fun beforeSpec(description: Description, spec: Spec) {

        println("before spec")
    }

    override fun afterSpec(description: Description, spec: Spec) {
        println("after spec")
    }

    override fun beforeTest(description: Description) {
        println("before test")
    }

    override fun afterTest(description: Description, result: TestResult) {
        println("after test")
    }

    var runCount = 0

    init {
        Given("a valid file specification") {
            println("given 1 ${runCount++}")

            When("we download it from S3") {
                println("when 1 ${runCount++}")

                Then("it is written to file") {
                    println("then 1 ${runCount++}")
                }
            }

            When("we download and rename it") {
                println("when 2 ${runCount++}")

                Then("the new file has the correct name") {
                    println("then 2 ${runCount++}")
                }

                Then("the new file has another correct name") {
                    println("then 2-2 ${runCount++}")
                }
            }
        }

        Given("a source that does not exist") {
            println("given 3 ${runCount++}")

            When("we download it from S3") {
                println("when 3 ${runCount++}")
                Then("it should throw") {
                    println("then 3 ${runCount++}")
                }
            }
        }
    }
}

class TemporaryDirectoryListener(val owner: Any) : TestListener {

    lateinit var tempDir: File

    override fun beforeSpec(description: Description, spec: Spec) {
        tempDir = createTempDir(prefix = owner::class.java.simpleName, suffix = "")
    }

    override fun afterSpec(description: Description, spec: Spec) {
        FileUtils.forceDelete(tempDir)
    }
}