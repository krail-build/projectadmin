package uk.q3c.build.creator.dir

import io.kotlintest.extensions.TestListener
import io.kotlintest.matchers.collections.shouldBeEmpty
import io.kotlintest.matchers.file.shouldBeADirectory
import io.kotlintest.matchers.file.shouldExist
import io.kotlintest.specs.BehaviorSpec
import io.mockk.every
import io.mockk.mockk
import uk.q3c.build.creator.Language
import uk.q3c.build.creator.ProjectBuilder
import uk.q3c.build.creator.SourceLanguage
import uk.q3c.build.creator.aws.TemporaryDirectoryListener
import uk.q3c.build.gitplus.local.GitLocal
import java.io.File


/**
 * Created by David Sowerby on 26 Sep 2018
 */
class DirectoryBuilderTest : BehaviorSpec() {

    override fun isInstancePerTest() = true
    val td = TemporaryDirectoryListener(this)

    override fun listeners(): List<TestListener> {
        return listOf(td)
    }


    init {
        val projectCreator: ProjectBuilder = mockk(relaxed = true)

        Given("All languages") {
            val temp = td.tempDir
            val projectDir: File = File(temp, "project")
            val local: GitLocal = mockk(relaxed = true)
            every { projectCreator.parentDir }.returns(temp)
            every { projectCreator.configuration.packageRoot }.returns("com.example")
            every { projectCreator.local }.returns(local)
            every { local.projectDir() }.returns(projectDir)
            val builder = DirectoryBuilder()
            builder.projectBuilder = projectCreator


            val javaDir = File(projectDir, "src/main/java")
            val kotlinDir = File(projectDir, "src/main/kotlin")
            val groovyDir = File(projectDir, "src/main/groovy")
            val resourcesDir = File(projectDir, "src/main/resources")

            val bpDir = "com/example"

            val javaDirBp = File(javaDir, bpDir)
            val groovyDirBp = File(groovyDir, bpDir)
            val kotlinDirBp = File(kotlinDir, bpDir)
            val resourcesBp = File(resourcesDir, bpDir)


            When("builder is configured and executed") {
                builder.configParam(SourceLanguage(Language.JAVA, "1.6"))
                builder.configParam(SourceLanguage(Language.KOTLIN, "1.0.3"))
                builder.configParam(SourceLanguage(Language.GROOVY, "2.6"))
                builder.execute()


                Then("directories are correctly created") {
                    javaDirBp.shouldExist()
                    javaDirBp.shouldBeADirectory()
                    javaDirBp.listFiles().asList().shouldBeEmpty()

                    kotlinDirBp.shouldExist()
                    kotlinDirBp.shouldBeADirectory()
                    kotlinDirBp.listFiles().asList().shouldBeEmpty()


                    groovyDirBp.shouldExist()
                    groovyDirBp.shouldBeADirectory()
                    groovyDirBp.listFiles().asList().shouldBeEmpty()


                    resourcesBp.shouldExist()
                    resourcesBp.shouldBeADirectory()
                    resourcesBp.listFiles().asList().shouldBeEmpty()

                }
            }
        }

    }
}