package uk.q3c.build.creator.gradle

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotlintest.matchers.collections.shouldContainAll
import io.kotlintest.matchers.types.shouldBeInstanceOf
import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec
import io.mockk.every
import io.mockk.mockk
import uk.q3c.build.creator.BaseVersion
import uk.q3c.build.creator.Language
import uk.q3c.build.creator.MavenPublishing
import uk.q3c.build.creator.ProjectConfiguration
import uk.q3c.build.creator.SourceLanguage
import uk.q3c.build.creator.TargetPathRoot.KOTLIN_BASE_PACKAGE
import uk.q3c.build.creator.TargetPathRoot.KOTLIN_SOURCE
import uk.q3c.build.creator.TestFramework
import uk.q3c.build.creator.TestSet
import uk.q3c.build.creator.template.Template
import uk.q3c.build.gitplus.gitplus.GitPlus
import uk.q3c.build.gitplus.local.GitLocal
import uk.q3c.build.gitplus.remote.GitRemote
import java.io.File


/**
 * Created by David Sowerby on 25 Sep 2018
 */
class ProjectConfigurationTest : BehaviorSpec() {

    init {
        Given("a default configuration") {
            val gitPlus: GitPlus = mockk(relaxed = true)
            val gitRemote: GitRemote = mockk(relaxed = true)
            val gitLocal: GitLocal = mockk(relaxed = true)
            every { gitPlus.local }.returns(gitLocal)
            every { gitPlus.remote }.returns(gitRemote)
            val configuration = ProjectConfiguration("test-project")
            val basePackage = "example.com"

            When("properties are set fluently") {
                configuration.basePackage(basePackage)

                Then("properties have new values") {
                    configuration.packageRoot.shouldBe(basePackage)
                }
            }

            When("we call template() with different parameters we get the correct template target path") {
                configuration.gitLocal = gitLocal
                configuration.basePackage("com.example.wiggly")
                val projectDir = File("home/projects/projectX")
                every { gitLocal.projectDir() } returns projectDir
                val kotlinSourceDir = File(projectDir, "src/main/kotlin")
                val kotlinBasePackageDir = File(kotlinSourceDir, "com/example/wiggly")
                val nestedPathDir = File(kotlinSourceDir, "com/example/wiggly/nested/path")
                configuration.template(sourcePath = "sourcePath", sourceFileName = "sourceFileName")
                configuration.template(sourcePath = "sourcePath", sourceFileName = "sourceFileName", targetPathRoot = KOTLIN_SOURCE)
                configuration.template(sourcePath = "sourcePath", sourceFileName = "sourceFileName", targetPathRoot = KOTLIN_BASE_PACKAGE)
                configuration.template(sourcePath = "sourcePath", sourceFileName = "sourceFileName", targetPathRoot = KOTLIN_BASE_PACKAGE, targetPath = "nested/path")

                Then("defaults are correct") {
                    with(configuration.steps[0] as Template) {
                        targetFileName.shouldBe("sourceFileName")
                        targetDir.shouldBe(projectDir)
                    }
                    with(configuration.steps[1] as Template) {
                        targetFileName.shouldBe("sourceFileName")
                        targetDir.shouldBe(kotlinSourceDir)
                    }
                    with(configuration.steps[2] as Template) {
                        targetFileName.shouldBe("sourceFileName")
                        targetDir.shouldBe(kotlinBasePackageDir)
                    }

                    with(configuration.steps[3] as Template) {
                        targetFileName.shouldBe("sourceFileName")
                        targetDir.shouldBe(nestedPathDir)
                    }
                }
            }

        }

        Given("we want to configure an empty configuration") {
            val baseVersion = "0.0.5.6"
            val configuration = ProjectConfiguration("test-project")

            When("base version is set") {
                configuration.baseVersion(baseVersion)

                Then("the step is created") {
                    configuration.steps.get(0).shouldBeInstanceOf<BaseVersion>()
                    (configuration.steps.get(0) as BaseVersion).baseVersion.shouldBe(baseVersion)
                }
            }

            When("maven publishing is requested") {
                configuration.mavenPublishing(true)
                Then("the step is created") {
                    configuration.steps.get(1).shouldBeInstanceOf<MavenPublishing>()
                    (configuration.steps.get(1) as MavenPublishing).useIt.shouldBe(true)
                }
            }

        }

        Given("Some duplicate test sets") {
            val configuration = ProjectConfiguration("test-project")
            val testSet1 = TestSet("test", TestFramework.JUNIT, "4.12")
            val testSet2 = TestSet("test", TestFramework.SPOCK, "1.2")
            val testSet3 = TestSet("test", TestFramework.SPOCK, "1.2")

            When("they are added") {
                configuration.testSet("test", TestFramework.JUNIT, "4.12")
                configuration.testSet("test", TestFramework.SPOCK, "1.2")
                configuration.testSet("test", TestFramework.SPOCK, "1.2")

                Then("the duplicates are not ignored") {
                    configuration.steps.size.shouldBe(3)
                    configuration.steps.shouldContainAll(testSet1, testSet2, testSet3)
                }
            }
        }


        Given("Some duplicate source sets") {
            val configuration = ProjectConfiguration("test-project")
            val source1 = SourceLanguage(Language.JAVA, "1.6")
            val source2 = SourceLanguage(Language.KOTLIN, "1.0.3")
            val source3 = SourceLanguage(Language.KOTLIN, "1.0.3")

            When("they are added") {
                configuration.source(Language.JAVA, "1.6")
                configuration.source(Language.KOTLIN, "1.0.3")
                configuration.source(Language.KOTLIN, "1.0.3")

                Then("the duplicates are not ignored") {
                    configuration.steps.size.shouldBe(3)
                    configuration.steps.shouldContainAll(source1, source2, source3)
                }
            }
        }

        Given("some configuration") {
            val configuration = ProjectConfiguration("test-project")
            configuration.source(Language.KOTLIN, "1.0.3")
            configuration.testSet("test", TestFramework.KOTLINTEST, "1.2")

            When("we serialise to Json and back") {
                val json = configuration.json()
                val mapper = jacksonObjectMapper()
                val returned: ProjectConfiguration = mapper.readValue(json)

                Then("the returned instance matches the original") {
                    returned.shouldBe(configuration)
                }
            }
        }


    }
}