package uk.q3c.build.creator

import com.google.common.collect.ImmutableList
import com.google.common.collect.ImmutableMap
import com.google.common.collect.ImmutableSet
import com.google.inject.Guice
import com.google.inject.Injector
import org.jetbrains.annotations.NotNull
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Ignore
import spock.lang.Specification
import uk.q3c.build.gitplus.gitplus.GitPlus
import uk.q3c.build.gitplus.local.DefaultGitLocalConfiguration
import uk.q3c.build.gitplus.local.GitLocal
import uk.q3c.build.gitplus.local.GitLocalConfiguration
import uk.q3c.build.gitplus.local.WikiLocal
import uk.q3c.build.gitplus.remote.DefaultGitRemoteConfiguration
import uk.q3c.build.gitplus.remote.GitRemote
import uk.q3c.build.gitplus.remote.GitRemoteConfiguration

/**
 * Created by David Sowerby on 10 Oct 2016
 */
class DefaultProjectBuilderTest extends Specification {

    @Rule
    TemporaryFolder temporaryFolder
    File temp

    MockBuilder builder1 = new MockBuilder()
    MockBuilder builder2 = new MockBuilder()
    ProjectConfiguration configuration = new ProjectConfiguration("test-project")
    ProjectBuilder creator
    GitPlus gitPlus = Mock(GitPlus)
    GitRemote gitRemote = Mock(GitRemote)
    GitLocal gitLocal = Mock()
    WikiLocal wikiLocal = Mock()

    GitRemoteConfiguration remoteConfiguration = new DefaultGitRemoteConfiguration()
    GitLocalConfiguration localConfiguration = new DefaultGitLocalConfiguration()
    GitLocalConfiguration wikiConfiguration = new DefaultGitLocalConfiguration()

    File projectDir
    BuilderConstructor builderConstructor = new DefaultBuilderConstructor()

    def setup() {
        temp = temporaryFolder.getRoot()
        projectDir = new File(temp, "wiggly")
        configuration.getGitPlus() >> gitPlus

        gitLocal.configuration >> localConfiguration
        gitRemote.configuration >> remoteConfiguration
        wikiLocal.configuration >> wikiConfiguration


        gitPlus.local >> gitLocal
        gitPlus.remote >> gitRemote
        gitPlus.wikiLocal >> wikiLocal
        configuration.getParentDir >> projectDir
    }


    def "module configuration"() {
        given:
        Injector injector = Guice.createInjector(new ProjectCreatorModule())


        when:

        creator = injector.getInstance(ProjectBuilder)
        creator.configuration = configuration

        then:
        creator instanceof DefaultProjectBuilder
        creator.buildersCount() == 3
    }

    def "execute calls each builder with each step"() {
        given:
        SourceLanguage sourceLanguage1 = new SourceLanguage(Language.JAVA, "1.8")
        SourceLanguage sourceLanguage2 = new SourceLanguage(Language.KOTLIN, "1.0.4")
        TestSet testSet1 = new TestSet("integrationTest", TestFramework.SPOCK, "groovy2.4")
        TestSet testSet2 = new TestSet("integrationTest", TestFramework.JUNIT, "4.10")
        MavenPublishing mavenPublishing = new MavenPublishing(true)
        List<Builder> builders = ImmutableList.of(builder1, builder2)
        List<ConfigStep> steps = ImmutableList.of(sourceLanguage1, testSet2, mavenPublishing, sourceLanguage2, testSet1)
        configuration.steps = steps
        configuration.builderClasses = new ArrayList()
        configuration.remoteProjectURI >> new URI("https://github.com/davidsowerby/wiggly")
        BuilderConstructor builderConstructor = Mock(BuilderConstructor)
        builderConstructor.buildersFor(configuration.builderClasses) >> builders
        creator = new DefaultProjectBuilder(gitPlus, builderConstructor)

        when:
        creator.configuration = configuration
        creator.invoke(Mock(GitLocalConfiguration))

        then:
        builder1.mavenPublishingCalled
        builder2.mavenPublishingCalled
        builder1.sourceLangugageCalled == 2
        builder2.sourceLangugageCalled == 2
        builder1.testSetCalled == 2
        builder2.testSetCalled == 2
        builder1.sourceLanguages.containsAll(sourceLanguage1, sourceLanguage2)
        builder2.sourceLanguages.containsAll(sourceLanguage1, sourceLanguage2)
        builder1.testSets.containsAll(testSet1, testSet2)
        builder2.testSets.containsAll(testSet1, testSet2)
        builder1.testSets.get(0) == testSet2
        builder2.testSets.get(0) == testSet2
        builder1.sourceLanguages.get(0) == sourceLanguage1
        builder2.sourceLanguages.get(0) == sourceLanguage1

        then:
        builder1.executeCalled == 1
        builder2.executeCalled == 1

    }

    def "delegation to project configuration"() {

        given:
        Set<Builder> builders = ImmutableSet.of(builder1, builder2)
        creator = new DefaultProjectBuilder(gitPlus, builderConstructor)
        File projectDir = new File("/home/temp")

        when:
        creator.configuration = configuration
        creator.configuration.setPackageRoot("x")
        creator.setParentDir(projectDir)

        then:
        creator.local.projectDirParent == projectDir
    }

    def "issue labels configuration passed to GitPlus"() {
        given:
        Injector injector = Guice.createInjector(new ProjectCreatorModule())
        creator = injector.getInstance(ProjectBuilder)
        Map<String, String> labels = ImmutableMap.of("a", "b")

        when:
        creator.remote.mergeIssueLabels(true)
        creator.remoteProjectURI = new URI("https://github.com/davidsowerby/wiggly")
        creator.remote.issueLabels(labels)

        then:
        creator.remote.mergeIssueLabels
        creator.remote.remoteProjectUri == "https://github.com/davidsowerby/wiggly"
        creator.remote.issueLabels == labels
    }


    @Ignore
    def "create a new project - for real"() {
        given:
        Injector injector = Guice.createInjector(new ProjectCreatorModule())
        creator = injector.getInstance(ProjectBuilder)
        creator.createNewProject = true
        creator.publicProject = true
        creator.remoteProjectURI = 'kaytee-test-delegate'
        creator.projectUserName = 'davidsowerby'
        creator.mergeIssueLabels = true
        creator.packageRoot = 'uk.q3c.kaytee'
        creator.useMavenPublishing = false
//        creator.baseVersion('0.0.0.1')
        creator.source(Language.JAVA, '').source(Language.KOTLIN, '')
//        creator.testSet('test', TestFramework.SPOCK, "")
        File gitDir = new File("/home/david/git")
        creator.parentDir = new File(gitDir, creator.getRemoteProjectURI)

        when:
        creator.execute()

        then:
        true

    }


    class MockBuilder implements Builder {


        ProjectBuilder creator
        int sourceLangugageCalled
        int testSetCalled
        int executeCalled
        boolean mavenPublishingCalled
        List<SourceLanguage> sourceLanguages = new ArrayList<>()
        List<TestSet> testSets = new ArrayList<>()

        @Override
        void execute() {
            executeCalled++
        }

        @Override
        void configParam(ConfigStep configStep) {
            if (configStep instanceof SourceLanguage) {
                configSourceLanguage(configStep)
            } else if (configStep instanceof TestSet) {
                configTestSet(configStep)
            } else if (configStep instanceof MavenPublishing) {
                mavenPublishingCalled = configStep.useIt
            }
        }

        void configSourceLanguage(@NotNull SourceLanguage step) {
            sourceLangugageCalled++
            sourceLanguages.add(step)
        }

        void configTestSet(@NotNull TestSet step) {
            testSetCalled++
            testSets.add(step)
        }


        @Override
        void projectCreator(@NotNull ProjectBuilder creator) {

        }
    }
}
