package uk.q3c.build.creator.git

import org.apache.commons.io.FileUtils
import org.apache.commons.io.filefilter.TrueFileFilter
import uk.q3c.build.creator.Builder
import uk.q3c.build.creator.ConfigStep
import uk.q3c.build.creator.ProjectBuilder
import java.io.File

/**
 * Created by David Sowerby on 10 Oct 2018
 */
class GitKeepBuilder : Builder {
    private lateinit var projectBuilder: ProjectBuilder

    override fun projectCreator(projectBuilder: ProjectBuilder) {
        this.projectBuilder = projectBuilder
    }

    override fun execute() {
        val root = projectBuilder.local.projectDir()
        val files = FileUtils.listFilesAndDirs(root, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE)
        val emptyDirectories = files.filter { it.isDirectory }.filter { it.listFiles().asList().isEmpty() }
        emptyDirectories.forEach { d ->
            val file = File(d, ".gitkeep")
            file.createNewFile()
        }
    }

    override fun configParam(configStep: ConfigStep) {
        // we don't need to do anything at config stage, all the work is in the execute() method
    }

}