package uk.q3c.build.creator

import uk.q3c.build.gitplus.local.GitLocalConfiguration
import uk.q3c.build.gitplus.remote.GitRemoteConfiguration
import java.io.File
import java.net.URI

/**
 * The implementation of this interface works in two stages - the first stage prepares by making configuration calls (for example to [source], [testSet] and [mergeIssueLabels]
 * These calls are delegated to [ProjectConfiguration]
 *
 * The [execute] method is then called to generate the required output, by providing the configuration to the [Builder]
 * implementations.  These builder implementations are defined by the Guice [ProjectCreatorModule]
 *
 * Note that for any action which requires authorised access to the remote repo, appropriate [API keys are required](http://gitplus.readthedocs.io/en/master/build-properties/).
 *
 * Created by David Sowerby on 10 Oct 2016
 */
interface ProjectBuilder {
    val configuration: ProjectConfiguration
    var templateCacheDirectory: File
    var s3BucketName: String
    var s3BucketPath: String
    val local: GitLocalConfiguration
    val remote: GitRemoteConfiguration
    val wiki: GitLocalConfiguration
    var remoteProjectURI: URI
    var parentDir: File
    fun buildersCount(): Int
    fun execute()
    fun configureFromJson(json: String)

}




enum class Language {
    JAVA, KOTLIN, GROOVY

}

enum class TestFramework {
    JUNIT, SPOCK, KOTLINTEST
}