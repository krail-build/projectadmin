package uk.q3c.build.creator.template

import org.apache.commons.io.FileUtils
import org.apache.velocity.VelocityContext
import org.apache.velocity.app.VelocityEngine
import org.apache.velocity.runtime.RuntimeConstants
import org.apache.velocity.tools.generic.EscapeTool
import org.slf4j.LoggerFactory
import uk.q3c.build.creator.Builder
import uk.q3c.build.creator.ConfigStep
import uk.q3c.build.creator.ProjectBuilder
import java.io.File
import java.io.StringWriter
import java.time.LocalDateTime
import java.util.*


/**
 * @param sourcePath relative to the S3 bucket or local cache root
 * @param targetDir relative to project root
 * @param targetFileName defaults to [sourceFileName], but if specified, the file is renamed
 */
data class Template(val sourcePath: String, val sourceFileName: String, val targetDir: File, val targetFileName: String = sourceFileName, val templateData: TemplateData) : ConfigStep


interface TemplateData

data class S3ObjectDescriptor(val bucket: String, val sourceFolder: String, val name: String) {
    val key: String get() = "$sourceFolder/$name"
    val path: String get() = "$bucket/$key"
}


class TemplateBuilder : Builder {
    private val log = LoggerFactory.getLogger(this.javaClass.name)
    var templates: MutableList<Template> = mutableListOf()
    private lateinit var projectBuilder: ProjectBuilder
    val velocityEngine = VelocityEngine()

    init {

    }


    override fun projectCreator(projectBuilder: ProjectBuilder) {
        this.projectBuilder = projectBuilder
        val properties = Properties()
        properties.setProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, projectBuilder.templateCacheDirectory.absolutePath)
        log.debug("Velocity template path set to ${projectBuilder.templateCacheDirectory.absolutePath}")
        velocityEngine.init(properties)

    }

    override fun execute() {
        templates.forEach { template ->
            val context = VelocityContext()
            context.put("data", template.templateData)
            context.put("esc", EscapeTool())
            val sw = StringWriter()

            val templateFileName = "${template.sourcePath}/${template.sourceFileName}"
            log.debug("Loading Velocity template $templateFileName")
            val templateFile = File(projectBuilder.templateCacheDirectory, templateFileName)
            val timeout = LocalDateTime.now().plusSeconds(5)
            while (!templateFile.exists() && LocalDateTime.now().isBefore(timeout)) {
                log.debug("waiting for file to exist: ${templateFile}")
                Thread.sleep(100)
            }
            if (!templateFile.exists()) {
                throw TemplateException("Unable to find template file at ${templateFile.absolutePath}")
            }


            velocityEngine.mergeTemplate(templateFileName, "UTF-8", context, sw)
            FileUtils.writeStringToFile(File(template.targetDir, template.targetFileName), sw.toString())

        }
    }

    override fun configParam(configStep: ConfigStep) {
        if (configStep is Template) {
            this.templates.add(configStep)
        }
        // ignore others
    }


}

class TemplateException(msg: String) : RuntimeException(msg)

class ProjectBuildException(msg: String) : RuntimeException(msg)
class AWSDownloadException(msg: String, e: Exception) : RuntimeException(msg, e)
class AWSDownloadTargetException(targetDir: File) : RuntimeException("Target directory $targetDir does not exist")

/**
 * @param collatorReference fully qualified reference, for example: uk.q3c.krail.DefaultBindingsCollator
 */
data class KrailBootstrapYmlTemplateData(val collatorReference: String) : TemplateData
