package uk.q3c.build.creator

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.common.collect.ImmutableList
import com.google.inject.Inject
import uk.q3c.build.creator.TargetPathRoot.PROJECT_ROOT
import uk.q3c.build.creator.dir.DirectoryBuilder
import uk.q3c.build.creator.git.GitKeepBuilder
import uk.q3c.build.creator.gradle.GradleGroovyBuilder
import uk.q3c.build.creator.gradle.task.GradleTask
import uk.q3c.build.creator.template.Template
import uk.q3c.build.creator.template.TemplateBuilder
import uk.q3c.build.creator.template.TemplateData
import uk.q3c.build.gitplus.local.GitLocalConfiguration
import java.io.File

/**
 * Created by David Sowerby on 13 Oct 2016
 */
data class ProjectConfiguration @Inject constructor(val projectName: String) {

    /**
     * Returns a JSON representation of the project configuration
     */
    fun json(): String {
        val mapper = jacksonObjectMapper()
        return mapper.writeValueAsString(this)
    }

    var builderClasses: List<Class<out Builder>> = listOf(GradleGroovyBuilder::class.java, DirectoryBuilder::class.java, GitKeepBuilder::class.java)

    var steps: MutableList<ConfigStep> = mutableListOf()

    @JsonIgnore
    lateinit var packageRoot: String

    @JsonIgnore
    lateinit var gitLocal: GitLocalConfiguration

    fun baseVersion(baseVersion: String): ProjectConfiguration {
        steps.add(BaseVersion(baseVersion))
        return this
    }

    fun basePackage(basePackage: String): ProjectConfiguration {
        this.packageRoot = basePackage
        return this
    }

    /**
     * Adds a test set (for example 'integrationTest') which allows [Builder]s to create appropriate directories and dependencies.
     * If you use multiple test frameworks for a test set, call this method multiple times
     *
     * @param setName the name of the test set
     * @param testFramework which test framework to use.  This enables identification of the appropriate dependencies
     * @param version the version of [testFramework] to use.  An empty String will cause the latest released version to be used
     */
    fun testSet(setName: String, testFramework: TestFramework, version: String): ProjectConfiguration {
        steps.add(TestSet(setName, testFramework, version))
        return this
    }

    /**
     * Use [template] to describe a file which should be copied into the project.  The [TemplateBuilder] determines how that is done,
     * depending on the implementation of [CodeTemplateLoader] used.
     *
     * The result of the template merge is sent to a file path constructed from:
     *
     * [targetPathRoot]/[targetPath]/[targetFileName]
     *
     * @param targetPathRoot the reference point for [targetPath]
     * @param targetPath where the file should be copied to, relative to the [targetPathRoot].  Defaults to ""
     * @param sourceFileName the template to copy, combined with [sourcePath]
     * @param sourcePath the relative path to the source folder for the template.  Use in combination with [sourceFileName]
     * @param targetFileName the new name for the copied template.  Defaults to [sourceFileName] with a suffix of ".vm" stripped off
     * @param templateData data to supply to Velocity to populate the template
     *
     */
    @Suppress("MemberVisibilityCanBePrivate")
    fun template(sourcePath: String, sourceFileName: String, targetPathRoot: TargetPathRoot = PROJECT_ROOT, targetPath: String = "", targetFileName: String = sourceFileName.removeSuffix(".vm"), templateData: TemplateData = EmptyTemplateData()): ProjectConfiguration {
        val targetPathRootDir = when (targetPathRoot) {
            PROJECT_ROOT -> gitLocal.projectDir()
            TargetPathRoot.KOTLIN_SOURCE -> File(gitLocal.projectDir(), "src/main/kotlin")
            TargetPathRoot.JAVA_SOURCE -> File(gitLocal.projectDir(), "src/main/java")
            TargetPathRoot.KOTLIN_TEST_SOURCE -> File(gitLocal.projectDir(), "src/test/kotlin")
            TargetPathRoot.JAVA_TEST_SOURCE -> File(gitLocal.projectDir(), "src/test/java")
            TargetPathRoot.KOTLIN_BASE_PACKAGE -> File(gitLocal.projectDir(), "src/main/kotlin/${packageRoot.replace(".", "/")}")
            TargetPathRoot.JAVA_BASE_PACKAGE -> File(gitLocal.projectDir(), "src/main/java/${packageRoot.replace(".", "/")}")
            TargetPathRoot.KOTLIN_TEST_BASE_PACKAGE -> File(gitLocal.projectDir(), "src/test/kotlin/${packageRoot.replace(".", "/")}")
            TargetPathRoot.JAVA_TEST_BASE_PACKAGE -> File(gitLocal.projectDir(), "src/test/java/${packageRoot.replace(".", "/")}")
        }

        val targetDir= if (targetPath == "" || targetPath == ".") {
            targetPathRootDir
        }else{
            File(targetPathRootDir, targetPath)
        }
        steps.add(Template(sourcePath = sourcePath, sourceFileName = sourceFileName, targetDir = targetDir, targetFileName = targetFileName, templateData = templateData))
        return this
    }

    fun mavenPublishing(value: Boolean): ProjectConfiguration {
        steps.add(MavenPublishing(value))
        return this
    }


    fun getSteps(): ImmutableList<ConfigStep> {
        return ImmutableList.copyOf(steps)
    }

    fun source(language: Language, version: String): ProjectConfiguration {
        steps.add(SourceLanguage(language, version))
        return this
    }

    fun gradleTask(taskName: String): ProjectConfiguration {
        steps.add(GradleTask(taskName))
        return this
    }

    /**
     * Project contains its own documentation structured for [ReadTheDocs](https://readthedocs.com)
     */
    fun readTheDocs() {
        val templateData = ReadTheDocsTemplateData(projectName = projectName)
        val sourcePath = "docs"
        template(sourcePath = sourcePath, sourceFileName = "index.rst", targetPath = "docs", templateData = templateData)
        template(sourcePath = sourcePath, sourceFileName = "conf.py", targetPath = "docs")
        template(sourcePath = sourcePath, sourceFileName = "Makefile", targetPath = "docs")
        template(sourcePath = sourcePath, sourceFileName = "specification.rst", targetPath = "docs", templateData = templateData)
    }


}

data class ReadTheDocsTemplateData(val projectName: String) : TemplateData
class EmptyTemplateData : TemplateData

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes(
        JsonSubTypes.Type(value = BaseVersion::class, name = "BaseVersion"),
        JsonSubTypes.Type(value = MavenPublishing::class, name = "MavenPublishing"),
        JsonSubTypes.Type(value = SourceLanguage::class, name = "SourceLanguage"),
        JsonSubTypes.Type(value = TestSet::class, name = "TestSet"),
        JsonSubTypes.Type(value = Template::class, name = "Template")
)
interface ConfigStep

data class SourceLanguage(val language: Language, val version: String) : ConfigStep {
    /**
     * Returns the specified version - or the default (latest) if the supplied [version] is blank
     */
    fun languageVersion(): String {
        return if (version.isBlank()) {
            when (language) {
                Language.JAVA -> "1.8"
                Language.KOTLIN -> "1.2.71"
                Language.GROOVY -> "2.4.8"
            }
        } else {
            version
        }
    }
}

data class TestSet(val setName: String, val testFramework: TestFramework, val version: String) : ConfigStep {

    fun frameworkVersion(): String {
        return if (version.isBlank()) {
            when (testFramework) {
                TestFramework.JUNIT -> "4.12"
                TestFramework.SPOCK -> "1.0-groovy-2.4"
                TestFramework.KOTLINTEST -> "3.1.9"
            }
        } else {
            version
        }
    }
}

data class BaseVersion(val baseVersion: String) : ConfigStep
data class MavenPublishing(val useIt: Boolean) : ConfigStep

enum class TargetPathRoot {
    PROJECT_ROOT, KOTLIN_SOURCE, JAVA_SOURCE, KOTLIN_TEST_SOURCE, JAVA_TEST_SOURCE, KOTLIN_BASE_PACKAGE, JAVA_BASE_PACKAGE, KOTLIN_TEST_BASE_PACKAGE, JAVA_TEST_BASE_PACKAGE
}


