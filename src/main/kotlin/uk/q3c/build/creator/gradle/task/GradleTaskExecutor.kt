package uk.q3c.build.creator.gradle.task

import org.gradle.tooling.GradleConnector
import org.slf4j.LoggerFactory
import uk.q3c.build.creator.Builder
import uk.q3c.build.creator.ConfigStep
import uk.q3c.build.creator.ProjectBuilder

/**
 * Created by David Sowerby on 25 Jan 2019
 */
data class GradleTask(val taskName: String) : ConfigStep


/**
 * This requires that Gradle is installed on the machine generating the project
 *
 * Created by David Sowerby on 24 Jan 2019
 */
class GradleTaskExecutor : Builder {
    private val log = LoggerFactory.getLogger(this.javaClass.name)
    private lateinit var projectBuilder: ProjectBuilder
    private val tasks = mutableListOf<String>()


    override fun configParam(configStep: ConfigStep) {
        if (configStep is GradleTask) {
            tasks.add(configStep.taskName)
        }
    }

    override fun execute() {
        if (tasks.isNotEmpty()) {
            val projectDir = projectBuilder.configuration.gitLocal.projectDir()
            log.debug("Connecting to Gradle project at $projectDir")
            val connector = GradleConnector.newConnector()

            connector.forProjectDirectory(projectDir)
            val connection = connector.connect()
            log.info("Gradle connected to build at $projectDir")


            val gradleLauncher = connection.newBuild()

            tasks.forEach { t ->
                log.debug("Executing gradle $t")
                gradleLauncher.forTasks(t).run()
            }

        }
    }


    override fun projectCreator(projectBuilder: ProjectBuilder) {
        this.projectBuilder = projectBuilder
    }
}