package uk.q3c.build.creator

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.inject.Inject
import org.slf4j.LoggerFactory
import uk.q3c.build.gitplus.gitplus.GitPlus
import uk.q3c.build.gitplus.local.GitLocalConfiguration
import uk.q3c.build.gitplus.local.ProjectCreator
import uk.q3c.build.gitplus.remote.GitRemoteConfiguration
import java.io.File
import java.net.URI

/**
 * Created by David Sowerby on 10 Oct 2016
 */
class DefaultProjectBuilder @Inject constructor(private val gitPlus: GitPlus, val builderConstructor: BuilderConstructor) : ProjectBuilder, ProjectCreator {
    override var s3BucketName: String = ""
    override var s3BucketPath: String = ""

    override fun configureFromJson(json: String) {
        this.configuration = jacksonObjectMapper().readValue(json)
    }

    override lateinit var configuration: ProjectConfiguration
    override lateinit var templateCacheDirectory: File
    private val log = LoggerFactory.getLogger(this.javaClass.name)
    override var remoteProjectURI: URI
        get() = URI(remote.remoteProjectUri)
        set(value) {
            remote.remoteProjectUri(value)
        }

    override var parentDir: File
        get() = local.projectDirParent
        set(value) {
            local.projectDirParent = value
        }

    override val local: GitLocalConfiguration = gitPlus.local.configuration
    override val remote: GitRemoteConfiguration = gitPlus.remote.configuration
    override val wiki: GitLocalConfiguration = gitPlus.wikiLocal.configuration

    var builders: List<Builder> = listOf()

    override fun execute() {
        gitPlus.propertiesFromGradle()
        gitPlus.local.projectCreator = this
        gitPlus.execute()
    }


    override fun buildersCount(): Int {
        return configuration.builderClasses.size
    }


    override fun invoke(configuration: GitLocalConfiguration) {
        log.info("creating local project in ${configuration.projectDir()}")
        builders = builderConstructor.buildersFor(this.configuration.builderClasses)
        for (builder in builders) {
            builder.projectCreator(this)
        }
        for (step in this.configuration.getSteps()) {
            for (builder in builders) {
                builder.configParam(step)
            }
        }
        for (builder in builders) {
            builder.execute() // create directories etc
        }

    }
}

interface BuilderConstructor {
    fun buildersFor(builderClasses: List<Class<out Builder>>): List<Builder>
}

class DefaultBuilderConstructor : BuilderConstructor {
    override fun buildersFor(builderClasses: List<Class<out Builder>>): List<Builder> {
        val builders: MutableList<Builder> = mutableListOf()
        builderClasses.forEach { bc -> builders.add(bc.newInstance()) }
        return builders
    }
}

