package uk.q3c.build.creator

import com.google.inject.AbstractModule
import com.google.inject.multibindings.Multibinder
import uk.q3c.build.creator.dir.DirectoryBuilder
import uk.q3c.build.creator.gradle.GradleGroovyBuilder
import uk.q3c.build.creator.template.TemplateBuilder
import uk.q3c.build.gitplus.GitPlusModule

/**
 * Created by David Sowerby on 10 Oct 2016
 */
class ProjectCreatorModule : AbstractModule() {

    private lateinit var builders: Multibinder<Builder>

    override fun configure() {
        install(GitPlusModule())
        builders = Multibinder.newSetBinder(binder(), Builder::class.java)
        builders.addBinding().toInstance(GradleGroovyBuilder())
        builders.addBinding().toInstance(DirectoryBuilder())
        builders.addBinding().toInstance(TemplateBuilder())
        bind(ProjectBuilder::class.java).to(DefaultProjectBuilder::class.java)
        bind(BuilderConstructor::class.java).to(DefaultBuilderConstructor::class.java)

    }
}
