package uk.q3c.build.creator.aws

import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.transfer.Download
import com.amazonaws.services.s3.transfer.TransferManagerBuilder
import org.slf4j.LoggerFactory
import uk.q3c.build.creator.template.AWSDownloadException
import uk.q3c.build.creator.template.AWSDownloadTargetException
import uk.q3c.build.creator.template.S3ObjectDescriptor
import java.io.File
import java.io.FileOutputStream


class AWSS3Reader {
    private val log = LoggerFactory.getLogger(this.javaClass.name)

    fun downloadFile(source: S3ObjectDescriptor, targetDir: File, targetFileName: String = ""): File {
        if (!targetDir.exists()) {
            throw AWSDownloadTargetException(targetDir)
        }
        try {
            val targetName = if (targetFileName.isEmpty()) {
                source.name.split("/").last()
            } else {
                targetFileName
            }
            val targetFile = File(targetDir, targetName)
            val s3 = s3()
            log.debug("Downloading object with key ${source.key} from bucket ${source.bucket}")
            val o = s3.getObject(source.bucket, source.key)
            o.objectContent.use { input ->
                FileOutputStream(targetFile).use { output ->
                    val readBuf = ByteArray(1024)

                    var done = false
                    while (!done) {
                        val readLength = input.read(readBuf)

                        if (readLength > 0) {
                            output.write(readBuf, 0, readLength)
                        } else {
                            done = true
                        }
                    }
                }
            }
            return targetFile
        } catch (e: Exception) {
            throw AWSDownloadException("failed to download object from ${source.bucket}/${source.key}", e)
        }


    }

    fun downloadFolder(bucketName: String, folderName: String, targetDir: File) {
        val objects = s3().listObjects(bucketName, folderName)
        val tx = TransferManagerBuilder.standard().withS3Client(s3()).build()
        val downloads: MutableList<Download> = mutableListOf()
        objects.objectSummaries.forEach {
            val download = tx.download(bucketName, it.key, File(targetDir, it.key))
            downloads.add(download)
        }
        var allDone = false
        log.debug("${objects.objectSummaries.size} objects identified for download")
        while (!allDone) {
            log.debug("Waiting for downloads to complete")
            allDone = true
            for (d in downloads) {
                if (!d.isDone) {
                    allDone = false
                    break
                }
            }
            Thread.sleep(1000)
        }

    }

    private fun s3(): AmazonS3 {
        return AmazonS3ClientBuilder.standard().withRegion(Regions.EU_WEST_2).build()
    }
}