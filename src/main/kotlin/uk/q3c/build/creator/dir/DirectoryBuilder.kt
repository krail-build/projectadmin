package uk.q3c.build.creator.dir

import org.apache.commons.io.FileUtils
import uk.q3c.build.creator.Builder
import uk.q3c.build.creator.ConfigStep
import uk.q3c.build.creator.Language
import uk.q3c.build.creator.ProjectBuilder
import uk.q3c.build.creator.SourceLanguage
import uk.q3c.build.creator.TestFramework
import uk.q3c.build.creator.TestSet
import uk.q3c.build.creator.template.Template
import java.io.File

/**
 * Creates directories and files as required by configuration calls to [configParam] methods
 *
 * Created by David Sowerby on 11 Oct 2016
 */
class DirectoryBuilder : Builder {


    lateinit var projectBuilder: ProjectBuilder
    val directories: MutableList<File> = mutableListOf()
    val files: MutableSet<FileCreator> = mutableSetOf()


    override fun execute() {
        for (dir in directories) {
            if (!dir.exists()) {
                FileUtils.forceMkdir(dir)
            }
        }

        for (fc in files) {
            fc.create()
        }
    }


    override fun configParam(configStep: ConfigStep) {
        when (configStep) {
            is SourceLanguage -> configSourceLanguage(configStep)
            is TestSet -> configTestSet(configStep)
            is Template -> configTemplate(configStep)
        }

    }

    private fun configTemplate(template: Template) {
        addDirectory(template.targetDir)
    }


    private fun addDirectory(targetDir: File) {
        directories.add(targetDir)
    }

    private fun configSourceLanguage(sourceLanguage: SourceLanguage) {
        when (sourceLanguage.language) {
            Language.JAVA -> {
                addDirectoryWithPackage("src/main/java")
            }
            Language.KOTLIN -> {
                addDirectoryWithPackage("src/main/kotlin")
            }
            Language.GROOVY -> {
                addDirectoryWithPackage("src/main/groovy")
            }
        }
        addDirectoryWithPackage("src/main/resources")
    }

    private fun addFile(file: File, content: String) {
        files.add(FileCreator(file, content))
    }

    /**
     * Creates and returns a directory for the provided directory, extended by the basePackagePath
     *
     */
    private fun addDirectoryWithPackage(directoryName: String): File {
        val main = File(projectBuilder.local.projectDir(), directoryName)
        directories.add(main)
        if (basePackageAsPath() != "") {
            val packageDir = File(main, basePackageAsPath())
            directories.add(packageDir)
            return packageDir
        } else {
            return main
        }
    }

    private fun configTestSet(testSet: TestSet) {
        val languageDir: String = when (testSet.testFramework) {

            TestFramework.JUNIT -> "java"
            TestFramework.SPOCK -> "groovy"
            TestFramework.KOTLINTEST -> "kotlin"
        }
        val testDirPath = "src/${testSet.setName}/$languageDir"
        addDirectoryWithPackage(testDirPath)
        addDirectoryWithPackage("src/test/resources")
    }

    private fun basePackageAsPath(): String {
        return projectBuilder.configuration.packageRoot.replace(".", "/")
    }

    override fun projectCreator(projectBuilder: ProjectBuilder) {
        this.projectBuilder = projectBuilder
    }
}

class FileCreator(val file: File, val content: String) {
    fun create() {
        FileUtils.write(file, content)
    }
}